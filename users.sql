-- Active: 1677011010340@@127.0.0.1@3306
CREATE DATABASE UsersIF NOT EXISTS `users` DEFAULT CHARACTER SET utf8 


CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(12) CHARACTER SET utf8 NOT NULL,
  `fname` varchar(20) CHARACTER SET utf8 NOT NULL,
  `lname` varchar(40) CHARACTER SET utf8 NOT NULL,
  `email` varchar(60) CHARACTER SET utf8 NOT NULL,
  `psword1` char(40) CHARACTER SET utf8 NOT NULL,
  `psword2` char(40) CHARACTER SET utf8 NOT NULL,
  `reg_date` datetime NOT NULL,
  `user_level` int(2) NOT NULL,
  `inputAddress` varchar(50) NOT NULL,
  `inputAddress2` varchar(50) NOT NULL,
  `inputCity` varchar(50) NOT NULL,
  `inputState` varchar(30) NOT NULL,
  `inputZip` varchar(10) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;


INSERT INTO `users' (`user_id`, 'title', 'fname', 'lname', 'email', 'psword1', 'psword2', 'reg_date', 'user_level', 'inputAddress', 'inputAddress2', 'inputCity', 'inputState') VALUE