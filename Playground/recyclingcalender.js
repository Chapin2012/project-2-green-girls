//This is an example of what a api for recycling calender might look like. 
//This is not a complete code structure. This is just from the research i have done thus far



fetch("https://api.example.com/recycling-calendar?location=1234")
  .then(response => response.json())
  .then(data => {
    console.log(data);
    const nextPickupDate = data.nextPickupDate;
    const pickupAffectedByHoliday = data.pickupAffectedByHoliday;
    // Update the HTML/CSS elements on the web page with the retrieved data
    document.getElementById("next-pickup-date").innerHTML = nextPickupDate;
    if (pickupAffectedByHoliday) {
      document.getElementById("pickup-status").innerHTML = "Pickup affected by holiday";
    } else {
      document.getElementById("pickup-status").innerHTML = "Normal pickup schedule";
    }
  })
  .catch(error => {
    console.error("Error fetching data from API:", error);
  });




  //Here's an example of how you can use the JavaScript code in your
 //HTML/CSS file to display the recycling calendar data:
 
//              <div>
 //                 <p>Next pickup date: <span id="next-pickup-date"></span></p>
 //                 <p>Pickup status: <span id="pickup-status"></span></p>
//              </div>

//              <script src="recycling-calendar.js"></script>
